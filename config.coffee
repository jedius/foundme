url = require("url")
module.exports = (env) ->
  config =
    port: 3000
    app:
      url: "http://localhost:3000"
      name: "fundme"
      port: 3000
      secretString: "secretString"
      maxAge: 3 * 60 * 60 * 1000

    log:
      level: "info"
      path: __dirname + "/server.log"

    smtp:
      mailer:
        service: "SendGrid"
        user: "support@babycarrot.tv"
        pass: 'weiwei123'

      templates:
        resetPassword:
          from: "admin"
          to: "user"
          subject: "Reset password on fundme.com"
          text: "text"
          html: "<b>best regarts </b><br>fundme developing team"

        confirmPassword:
          from: "admin"
          subject: "Confirm password on fundme.com"
          text: "text"

    db:
      redisURL: url.parse(process.env.REDISTOGO_URL or "redis://localhost:6379")
      mongoUrl: process.env.MONGOHQ_URL or "mongodb://localhost/household"

    validate:
      email: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
      string: /^([a-zA-Z]){1}([a-zA-Z0-9]){0,48}$/
      stringWithSpace: /^([a-zA-Z]){1}([ a-zA-Z0-9]){0,48}$/
      objectId: /^([a-z0-9]){24}$/
      password: /.{5,20}/
      url: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/

  switch env
    when "production"
      config.port = process.env.PORT or 5000
      config.app.url = "http://fundme.herokuapp.com"
      config.db.mongoUrl = process.env.MONGOHQ_URL or "mongodb://localhost/fundme"
      break
    when "development"
      config.db.mongoUrl = process.env.MONGOHQ_URL or "mongodb://localhost/fundme"
      break
    when "testing"
      config.db.mongoUrl = "mongodb://localhost/fundme_testing"
      break
    else
      break
  config
