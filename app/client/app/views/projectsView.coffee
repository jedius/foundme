ProjectsView = Backbone.View.extend
  
  events:
    'click .project': 'openProject'

  initialize: ->

  render: ()->
    @collection.fetch
      success: =>
        @$el.html _.template(window.Templates.projects, projects: @collection.toJSON())
        $('.appModule').hide();
        @$el.show();
        if window.app.user.get('id')
          @$('.create-project-button').show()
        window.app.user.on 'change', =>
          @$('.create-project-button').show()

  openProject: (e)->
    id = $(e.currentTarget).attr('_id')
    window.app.router.navigate("project/#{id}", trigger: true)