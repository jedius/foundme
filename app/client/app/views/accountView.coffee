AccountView = Backbone.View.extend
  
  events:
    'keyup #account-year': 'calcCustomYear'


  initialize: ->

  render: ->
    @template = _.template window.Templates.account,
      data: @model.get('data')
      total: @model.get('total')
      calc: @calc

    @$el.html @template
    $('.appModule').hide();
    @$el.show();

  calc: (total,year)->
    try
      y = parseInt year

    if y
      y--
      total = total + total*0.03
      if y > 0
        total = @calc(total,y)
    total = parseFloat(total).toFixed(2)
    return total

  calcCustomYear: ->
    year = parseInt @$('#account-year').val()
    amount = @calc @model.get('total'),year
    console.log(amount)
    @$('#account-year-result').text('$'+amount)
    @$('#account-year-result-part').text('$'+(amount*0.2).toFixed(2))