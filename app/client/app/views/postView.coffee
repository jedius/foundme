PostView = Backbone.View.extend

  events:
  	'submit form': 'createProject'
  
  initialize: ->

  render: (id)->
    switch id
      when 'robotic'
        data =
          name: 'Robotic Arm Kit'
          message: 'from robot-r-us.com'
          amount: '$125.00'
      when 'harry'
        data =
          name: 'Harry Potter completed set'
          message: 'from Ace Book Store - SG'
          amount: '$150'
      when 'bicycle'
        data =
          name: 'Children Bicycle'
          message: 'from Child Safe Bike - SG'
          amount: '$75'
    if data
      @template = _.template(window.Templates.postPreset, data: data)
    else
      @template = _.template(window.Templates.post)
    @$el.html @template
    $('.appModule').hide();
    @$el.show();

  createProject: (e)->
  	data = @$('form').serializeObject()
  	@collection.create data,
  		success: (model)->
        id = model.get('_id')
        window.app.router.navigate("project/#{id}", trigger: true)
  	return false