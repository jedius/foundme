ProjectView = Backbone.View.extend
  events:
	  'submit form.message-form': 'sendMessage'
	  'submit form.fund-form': 'fund'
	  'submit form.invite-form': 'invite'

  initialize: ->

  render: ->
    @model.fetch
      success: =>
        @$el.html _.template window.Templates.project, 
          project: @model.toJSON()
        $('.appModule').hide();
        @$el.show();
  
  sendMessage: ->
    data = @$('form.message-form').serializeObject()
    @$('form.message-form')[0].reset()
    $.post '/message/'+@model.get('_id'), data, (res)->
    false

  fund: ->
    data = @$('form.fund-form').serializeObject()
    @$('form.fund-form')[0].reset()
    $.post '/fund/'+@model.get('_id'), data, (res)->
    false  	
  
  invite: ->
    data = @$('form.invite-form').serializeObject()
    @$('form.invite-form')[0].reset()
    $.post '/invite/'+@model.get('_id'), data, (res)->
    false