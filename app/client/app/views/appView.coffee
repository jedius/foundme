AppView = Backbone.View.extend

  initialize: ->
    @router = new Router()
    @user = new UserModel()
    if window.user and window.user.id
      @user.set(window.user)
    @user.on 'change', =>
      @renderPanel()

  render: ->
    @$el.html _.template(window.Templates.app)
    @renderPanel()
    @router.start()

  renderPanel: ->
    @$('#navbar').html _.template(window.Templates.navbar, user: @user.toJSON())