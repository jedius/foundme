LoginView = Backbone.View.extend
  
	events: 
		"submit form": "login"

	initialize: ->

	render: ->
	    @template = _.template(window.Templates.login)
	    @$el.html @template
	    $('.appModule').hide();
	    @$el.show();

	login: ->
		data = @$('form').serializeObject()
		$.ajax
			type: "POST"
			url: "/login"
			data: data
			success: (res) =>
				if res.code is 200
					window.app.user.set(res.user)
					window.app.router.navigate('account', trigger: true)
				else
					@$('.login-alert').show()
		false