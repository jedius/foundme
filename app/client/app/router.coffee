Router = Backbone.Router.extend
  routes:
    "projects": "projects"
    "project/:id": "project"
    "post": "post"
    "post/:id": "postPreset"
    "account": "account"
    "login": "login"
    "*actions": "defaultAction"

  initialize: ->
    @projectsCollection = new ProjectCollection
    @views = 
      project: []

  start: ->
    Backbone.history.start()

  projects: ->
    unless @views.projects
      @views.projects = new ProjectsView
        el: $('<div id=projects class=appModule />').appendTo($('#body'))
        collection: @projectsCollection
    @views.projects.render()
  
  post: ->
    unless @views.post
      @views.post = new PostView
        el: $('<div id=post class=appModule />').appendTo($('#body'))
        collection: @projectsCollection
    @views.post.render()

  postPreset: (id)->
    unless @views.post
      @views.post = new PostView
        el: $('<div id=post class=appModule />').appendTo($('#body'))
        collection: @projectsCollection
    @views.post.render(id)

  project: (id)->
    unless @views.project[id]
      model = new ProjectModel
        _id: id
      @views.project[id] = new ProjectView
        el: $("<div id=project_#{id} class=appModule />").appendTo($('#body'))
        model: model          
    @views.project[id].render()
  
  account: ->
    unless @views.account
      @views.account = new AccountView
        el: $('<div id=about class=appModule />').appendTo($('#body'))
        model: new AccountModel
    @views.account.render()
  
  login: ->
    unless @views.login
      @views.login = new LoginView
        el: $('<div id=login class=appModule />').appendTo($('#body'))
    @views.login.render()

  defaultAction: ->
    @navigate 'projects', trigger: true

