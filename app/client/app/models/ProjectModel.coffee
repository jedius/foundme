ProjectModel = Backbone.Model.extend
	url: ->
		id = this.get('_id')
		if id
			'/projects/'+id
		else
			'/projects'

	initialize: ->
