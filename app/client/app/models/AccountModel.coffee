AccountModel = Backbone.Model.extend

  initialize: ->
    @data = [
      name: 'Grandpa Joe'
      payments: [
        date: 'May-11'
        amount: 50
      ,
        date: 'Jun-11'
        amount: 40 
      ,
        date: 'Dec-11'
        amount: 30 
      ,
        date: 'May-12'
        amount: 20 
      ,
        date: 'Dec-12'
        amount: 17
      ,
        date: 'Mar-13'
        amount: 50 
      ]
    ,
      name: 'Grandma Tan'
      payments: [
        date: 'May-11'
        amount: 30
      ,
        date: 'Jun-11'
        amount: 40 
      ,
        date: 'Dec-11'
        amount: 20 
      ,
        date: 'May-12'
        amount: 70 
      ,
        date: 'Dec-12'
        amount: 36
      ,
        date: 'Mar-13'
        amount: 74 
      ]
    ,
      name: 'Grandpa Li'
      payments: [
        date: 'May-11'
        amount: 60
      ,
        date: 'Jun-11'
        amount: 20 
      ,
        date: 'Dec-11'
        amount: 10 
      ,
        date: 'May-12'
        amount: 70 
      ,
        date: 'Dec-12'
        amount: 90
      ,
        date: 'Mar-13'
        amount: 40 
      ]
    ,
      name: 'Grandpa Li'
      payments: [
        date: 'May-11'
        amount: 24
      ,
        date: 'Jun-11'
        amount: 36
      ,
        date: 'Dec-11'
        amount: 66 
      ,
        date: 'May-12'
        amount: 5 
      ,
        date: 'Dec-12'
        amount: 59
      ,
        date: 'Mar-13'
        amount: 98 
      ]
    ,
      name: 'Grandma Rose'
      payments: [
        date: 'May-11'
        amount: 54
      ,
        date: 'Jun-11'
        amount: 23 
      ,
        date: 'Dec-11'
        amount: 87 
      ,
        date: 'May-12'
        amount: 58 
      ,
        date: 'Dec-12'
        amount: 59
      ,
        date: 'Mar-13'
        amount: 52 
      ]
    ,
      name: 'Mom'
      payments: [
        date: 'May-11'
        amount: 30
      ,
        date: 'Jun-11'
        amount: 40 
      ,
        date: 'Dec-11'
        amount: 90 
      ,
        date: 'May-12'
        amount: 40 
      ,
        date: 'Dec-12'
        amount: 27
      ,
        date: 'Mar-13'
        amount: 20 
      ]
    ,
      name: 'Uncle Li'
      payments: [
        date: 'May-11'
        amount: 50
      ,
        date: 'Jun-11'
        amount: 80 
      ,
        date: 'Dec-11'
        amount: 30 
      ,
        date: 'May-12'
        amount: 20 
      ,
        date: 'Dec-12'
        amount: 27
      ,
        date: 'Mar-13'
        amount: 40 
      ]
    ,
      name: 'Aunty Li'
      payments: [
        date: 'May-11'
        amount: 60
      ,
        date: 'Jun-11'
        amount: 70 
      ,
        date: 'Dec-11'
        amount: 80 
      ,
        date: 'May-12'
        amount: 70 
      ,
        date: 'Dec-12'
        amount: 87
      ,
        date: 'Mar-13'
        amount: 90 
      ]
    ]
    total = 0
    for d in @data
      d.subtotal = 0
      for p in d.payments
        d.subtotal += p.amount
        total += p.amount
    @set 'data', @data
    @set 'total', total