logFrom = "Auth passport"
passport = glob.modules.passport
LocalStrategy = glob.modules.passportLocal.Strategy

passport.serializeUser (user, done) ->
  done null, user  if user

passport.deserializeUser (user, done) ->
  if user
    mongo.user.findById user.id, (err, user) ->
      unless err
        if user
          done null, user
        else
          log.warn logFrom, "user not found"
          done null, false
      else
        console.log "error: ", err
        done err, false

  else
    log.info logFrom, "id not found"

passport.use "local", new LocalStrategy({
    usernameField: 'email'
    passwordField: 'password'
  }, (email, password, done) ->
    console.log('login strategy');
    console.log(email,password);
    unless glob.config.validate.email.test(email)
      console.log('invalid');
      done 'invalid email',null,
        message: 'invalid email'
      return
    mongo.user.findOne
      email: email
    , (err, user) ->
      unless err
        if user
          console.log('user',user);
          # check password
          if user.password is password
            #logged in
            console.log('logged in');
            done null, user.entity
          else
              err = 'email already exist and password is wrong'
              log.warn logFrom, err
              console.log('why');
              done null, null,
                code: 1
        else
          # create new user
          nuser = new mongo.user
            email: email
            password: password
          nuser.save (err,user)->
            unless err
              if user
                console.log "join success",user
                done null, user.entity
              else
                console.log "no user? oO"
                done 'no user', false
            else
              console.log "error",err
              done err, false
      else
        log.warn logFrom, "user find error"
        done err, false

)