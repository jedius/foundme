console.log "server start"

global.glob = {}
glob.modules =
  http: require("http")
  url: require("url")
  fs: require("fs")
  child_process: require("child_process")
  util: require("util")
  passport: require("passport")
  passportLocal: require("passport-local")
  mongoose: require("mongoose")
  express: require("express")
  crypto: require("crypto")
  request: require("request")
  winston: require("winston")
  redis: require("redis")
  nodemailer: require("nodemailer")
  moment: require("moment")

glob.utils = require("./utils")
glob.spawn = glob.modules.child_process.spawn

module.exports.start = (env, cb) ->
  env = env or "development"
  glob.config = require("../../config")(env)
  glob.modules.winston.addColors
    info: "blue"
    warn: "yellow"
    error: "red"

  glob.smtpTransport = glob.modules.nodemailer.createTransport("SMTP",
    service: glob.config.smtp.mailer.service
    auth:
      user: glob.config.smtp.mailer.user
      pass: glob.config.smtp.mailer.pass
  )
  glob.email = glob.smtpTransport.sendMail
  
  global.log = new glob.modules.winston.Logger(
    transports: [new glob.modules.winston.transports.Console(
      level: glob.config.log.level
      colorize: "true"
    ), new glob.modules.winston.transports.File(filename: glob.config.log.path)]
    exitOnError: false
  )
  global.mongo = require("./mongo")
  express = glob.modules.express
  app = express()
  
  #redis
  glob.redis = glob.modules.redis.createClient(glob.config.db.redisURL.port, glob.config.db.redisURL.hostname)
  glob.redis.auth glob.config.db.redisURL.auth.split(":")[1]  if glob.config.db.redisURL.auth
  sessionStore = require("connect-redis")(express)
  glob.sessionStore = new sessionStore(
    client: glob.redis
    prefix: "framejed:"
  )
  
  app.configure ->
    app.set "views", __dirname + "/views"
    app.set "port", glob.config.port
    app.set "env", env
    app.set "view engine", "jade"
    app.use express.favicon()
    app.use express.logger()
    app.use express.cookieParser()
    app.use express.session(
      secret: glob.config.app.secretString
      # store: glob.sessionStore
      cookie:
        maxAge: glob.config.app.maxAge
    )
    app.use express.bodyParser()
    app.use express.methodOverride()
    app.use glob.modules.passport.initialize()
    app.use glob.modules.passport.session()
    
    require "./passport"
    app.use app.router
    app.use express["static"](__dirname + "/../public")

  app.configure "development", ->
    app.use express.errorHandler(
      dumpExceptions: true
      showStack: true
    )

  app.configure "production", ->
    app.use express.errorHandler(
      dumpExceptions: true
      showStack: true
    )

  
  require("./router") app


  glob.modules.mongoose
    .connect glob.config.db.mongoUrl, (err) ->
      if err
        console.log "mongo err:"
        console.log err
      else
        require('./build').start ->
          glob.modules.http
            .createServer(app)
            .listen app.get('port'), ->
              log.info "Server start on port " + app.get('port') + " in " + app.settings.env + " mode"
              cb()  if cb


