module.exports = (app) ->

	app.get "/", (req, res) ->
	  id = ''
	  email = ''
	  if req.isAuthenticated() and req.user
	  	id = req.user.id
	  	email = req.user.email
	  res.render "index.jade",
	    title: "fundme"
	    id: id
	    email: email


	require("./auth") app
	require("./project") app
	require("./actions") app