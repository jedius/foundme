passport = glob.modules.passport
logFrom = "auth router: "
module.exports = (app) ->
  app.post "/login", (req, res) ->
    passport.authenticate("local", (err, user, info) ->
      unless err
        if user
          req.logIn user, (err) ->
            if err
              res.send
                code: 500
                message: err
              next err
            else
              res.send
                code: 200
                user: user
        else
          res.send
            code: 400
            error: "wrong login or password"

      else
        console.log('err',err);
        res.send
          code: 400
          error: "login error"

    )(req, res)

  # mail = glob.config.smtp.templates.confirmPassword
  # mail.to = user.email
  # mail.html = "Welcome to Household. For complete the registration you need to login by using One Time Password: <h2>" + user.otp + "</h2><a href=" + acceptLink + ">HERE</a><br><br>If this message is sent to you by mistake, click on this link <a href=" + denyLink + ">here</a>, or ignore this message.<br><br><br> With love Household developing team"
  # smtpTransport.sendMail mail, (error, response) ->

  app.get "/logout", (req, res) ->
    console.log('logout');
    req.logout ->
      console.log('logout callback?');
    res.redirect "/"
