module.exports = (app)->
  app.get '/projects', (req,res) ->
    mongo.project.find().skip(0).limit(20).exec (err,projects)->
      if err
        console.log(err);
      else
        res.send projects

  # create project
  app.post '/projects', (req,res) ->
      project = new mongo.project(req.body)
      if req.user
        project.userId = req.user.id
        project.email = req.user.email
      project.save (err)->
        console.log(err) if err
        res.send project

  app.get '/projects/:id', (req,res) ->
    console.log('get project');
    mongo.project.findById req.params.id, (err,project)->
      if err
        console.log(err);
        res.send 400
      if project
        res.send project
      else
        res.send 400

  app.put '/projects/:id', (req,res) ->
    console.log('update project');
    mongo.project.update {_id: req.params.id}, req.body, {}, (err,n)->
      if err
        console.log(err);
      console.log(n);
      res.send 200

  app.delete '/projects/:id', (req,res) ->
    console.log('delete project');
    mongo.project.findById req.params.id, (err,project)->
      if err
        console.log(err);
        res.send 400
      if project
        project.remove (err)->
          if err
            console.log(err);
          res.send 200
      else
        res.send 400