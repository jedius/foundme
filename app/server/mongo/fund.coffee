mongoose = glob.modules.mongoose
schema = new mongoose.Schema

  userId:
    type: "string"

  projectId:
    type: "string"

  message:
    type: "string"

  amount:
    type: "string"

module.exports = mongoose.model("fund", schema)