mongoose = glob.modules.mongoose
schema = new mongoose.Schema

  userId:
    type: "string"

  projectId:
    type: "string"

  message:
    type: "string"

  emails:
    type: "array"

module.exports = mongoose.model("invite", schema)