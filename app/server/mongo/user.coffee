mongoose = glob.modules.mongoose
user = new mongoose.Schema(
  email:
    type: "string"

  name:
    type: "string"

  role:
    type: "string"

  password:
    type: "string"

  gen:
    type: "string"
    enum: ["m", "f"]

  birth:
    type: "string"

  phone:
    type: "string"

  city:
    type: "string"

  otp:
    type: "string"

  zip:
    type: "number"

  country:
    type: "string"

  confirmation:
    type: "bool"
    default: false

  dt:
    type: "date"
    default: Date.now()

  lm:
    type: "date"
    default: Date.now()
)

user.virtual("entity").get ->
  entity =
    id: @_id.toString()
    email: @email
    name: @name
    role: @role
    house: @house
    gen: @gen
    birth: @birth
    phone: @phone
    address: @address
    city: @city
    otp: @otp
    zip: @zip
    country: @country
    confirmation: @confirmation
    dt: @dt
    lm: @lm

  entity

module.exports = mongoose.model("user", user)