mongoose = glob.modules.mongoose
schema = new mongoose.Schema

	userId:
		type: "string"

	email:
		type: "string"

	name:
		type: "string"

	amount:
		type: "string"

	message:
		type: "string"

	getFunding:
		type: "string"

	deliver:
		type: "string"

module.exports = mongoose.model("project", schema)